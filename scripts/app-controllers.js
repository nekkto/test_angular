var appControllers = angular.module('appControllers', []);

appControllers.controller('mainController', function($scope, $http, filterPresetService) {
    
    var apiUrl = 'http://stage.2g-it.ru/api';
    $scope.tasks = [];
    $scope.statusNames = {'new': 'Новая', 'progress': 'В работе', 'done': 'Выполнена', 'check': 'На проверке', 'date': 'Перенос срока'};
    $scope.columns = {
        'name': {
            'label': 'Название',
            'sortable': false,
        },
        'description': {
            'label': 'Описание',
            'sortable': false,
        },
        'date': {
            'label': 'Дата исполнения',
            'sortable': true,
        },
        'status': {
            'label': 'Статус',
            'sortable': true,
        },
        'authorName': {
            'label': 'Имя автора',
            'sortable': false,
        },
    };
    $scope.params = {
        'pageCount': 0,
        'pages': [0, 1],
        //'selectedFilterPreset': 'all',
    };
    $scope.filterPresets = filterPresetService;
    
    $scope.getTrClass = function(filter) {
        return (filter.hidden) ? 'display-none' : '';
    };
    
    $scope.getThClass = function(column) {
        return column == $scope.filters.sort.value && 'sort-' + ($scope.filters.sortType.value == 'DESC');
    };
    
    $scope.getPbClass = function(page) {
        return page == $scope.filters.pageNum.value && 'page-button-selected';
    };
    
    $scope.getFilterPresetClass = function(preset) {
        return preset == $scope.params.selectedFilterPreset && 'page-button-selected';
    };
    
    $scope.getStatusName = function(status) {
        var name = $scope.statusNames[status];
        if (typeof name == 'undefined')
            name = status;
        return name;
    };
    
    $scope.setSorting = function(column) {
        if ($scope.columns[column].sortable === false)
            return false;
        if ($scope.filters.sort.value == column) {
            $scope.filters.sortType.value = ($scope.filters.sortType.value == 'DESC') ? 'ASC' : 'DESC';
        } else {
            $scope.filters.sort.value = column;
            $scope.filters.sortType.value = 'DESC';
        }
        $scope.getTasks();
    };
    
    $scope.setFilterPreset = function(preset) {
        $scope.filters = $scope.filterPresets[preset].filters;
        $scope.params.selectedFilterPreset = preset;
        $scope.filters.pageNum.value = 1;
        $scope.getTasks();
    };
    
    $scope.setFilter = function(filters) {
        $scope.filters.pageNum.value = 1;
        $scope.getTasks();
    };
    
    $scope.viewPage = function(page) {
        $scope.filters.pageNum.value = page;
        $scope.getTasks();
    };
    
    $scope.auth = function(callBack) {
        $http({
            method : "POST",
            url: apiUrl + "/user/login",
            data: {
                'email': 'task@test.api',
                'password': 'qwerty',
            }
        }).then(function mySucces(response) {
            $scope.myWelcome = response.data;
            callBack();
        }, function myError(response) {
            alert('При запросе произошла ошибка: ' + response.status + ' ' + response.statusText);
        });
    };
    
    $scope.getTasks = function() {
        //var filterParams = $scope.getFilterParams();
        var serializedParams = $scope.serialize( $scope.getFilterParams() );
        $http({
            method : 'GET',
            url: apiUrl + '/tasks?' + serializedParams,
            //data: serializedParams,
            //params: filterParams //data: 'limit=5&skip=0&sort=date&sortType=DESC&status=new&status=progress'
        }).then(function mySucces(response) {
            $scope.params.pageCount = Math.ceil(response.data.pagination.count / $scope.filters.limit.value);
            $scope.params.pages = [];
            for (var i = 0; i < $scope.params.pageCount; i++)
                $scope.params.pages[i] = i;
            $scope.drawTable(response.data.data);
        }, function myError(response) {
            alert('При запросе произошла ошибка: ' + response.status + ' ' + response.statusText);
        });
    };
    
    $scope.drawTable = function(tasks) {
        $scope.tasks = [];
        for (k in tasks) {
            $scope.tasks.push({
                'name': tasks[k].name,
                'description': tasks[k].task,
                'date': tasks[k].date,
                'status': $scope.getStatusName([tasks[k].status]),
                'authorName': tasks[k].author.name,
            });
        }
    };
    
    $scope.getFilterParams = function() {
        var params = [];
        //отключаем показ задач без дедлайнов если установлены даты дедлайнов
        if ($scope.filters.dateTo.value || $scope.filters.dateFrom.value)
            $scope.filters.no_date.value = ''; //no_date=0 не работает на бекенде, т.е. по идее должен отключать показ задач без дедлайнов
        for (k in $scope.filters) {
            if ($scope.filters[k].value)
                params[k] = $scope.filters[k].value;
        }
        params['skip'] = ($scope.filters.pageNum.value - 1) * $scope.filters.limit.value;
        delete params['pageNum'];
        return params;
    };
    
    $scope.serialize = function(obj, propName) {
        var serializedString = '';
        for (var prop in obj)
            if (obj.hasOwnProperty(prop)) {
                var delimeter = (serializedString.length) ? '&' : '';
                if (typeof obj[prop] == 'object' && obj[prop].length > 0) {
                    if (obj[prop].length === 1)
                        serializedString += delimeter + encodeURIComponent(prop) + '[]=' + encodeURIComponent(obj[prop]);
                    else 
                        serializedString += delimeter + this.serialize(obj[prop], encodeURIComponent(prop) + '[]');
                } else {
                    var name = (typeof propName == 'undefined') ? encodeURIComponent(prop) : propName;
                    serializedString += delimeter + name + '=' + encodeURIComponent(obj[prop]);
                }
            }
        return serializedString;
    };
    
    $scope.setFilterPreset('all');
    $scope.auth($scope.getTasks);
});