var appServices = angular.module('appServices', []);

appServices.service('filterPresetService', function() {
return {
    'all': {
        'label': 'Все фильтры',
        'filters': {
            'responsible': {
                'label': 'ID исполнителя',
            },
            'author': {
                'label': 'ID постановщика',
            },
            'dateFrom': {
                'label': 'Срок исполнения задачи С',
                'type': 'date',
            },
            'dateTo': {
                'label': 'Срок исполнения задачи ПО',
                'type': 'date',
            },
            'pageNum': {
                'label': 'Номер страницы',
                'value': 1,
            },
            'sort': {
                'label': 'Поле сортировки',
                'type': 'select',
                'value': 'date',
                'optionValues': ['id', 'date', 'updatedAt', 'status'],
            },
            'sortType': {
                'label': 'Тип сортировки',
                'type': 'select',
                'value': 'DESC',
                'optionValues': ['DESC', 'ASC'],
            },
            'status': {
                'label': 'Статусы',
                'type': 'multiSelect',
                'value': [],
                'optionValues': ['new', 'progress', 'done', 'check', 'date'],
                'multipleSelect': true,
            },
            'no_date': {
                'label': 'Показывать задачи без дат',
                'type': 'select',
                'value': '',
                'optionValues': ['', '1'],
            },
            'limit': {
                'label': 'Записей на странице',
                'value': 5,
            },
        },
    },
    'set_1': {
        'label': 'Задачи для пользователя',
        'filters': {
            'responsible': {
                'label': 'ID исполнителя',
                'value': '',
            },
            'author': {
                'label': 'ID постановщика',
                'hidden': true,
                'value': '',
            },
            'dateFrom': {
                'label': 'Срок исполнения задачи С',
                'type': 'date',
            },
            'dateTo': {
                'label': 'Срок исполнения задачи ПО',
                'type': 'date',
            },
            'pageNum': {
                'label': 'Номер страницы',
                'value': 1,
                'hidden': true,
            },
            'sort': {
                'label': 'Поле сортировки',
                'type': 'select',
                'value': 'date',
                'optionValues': ['date'],
                'hidden': true,
            },
            'sortType': {
                'label': 'Тип сортировки',
                'type': 'select',
                'value': 'DESC',
                'optionValues': ['DESC', 'ASC'],
                'hidden': true,
            },
            'status': {
                'label': 'Статусы',
                'type': 'multiSelect',
                'value': [],
                'optionValues': ['new', 'progress', 'done', 'check', 'date'],
                'multipleSelect': true,
                'hidden': true,
            },
            'no_date': {
                'label': 'Показывать задачи без дат',
                'type': 'select',
                'value': '',
                'optionValues': ['', '1'],
                'hidden': true,
            },
            'limit': {
                'label': 'Записей на странице',
                'value': 5,
                'hidden': true,
            },
        },
    },
    'set_2': {
        'label': 'Задачи от пользователя',
        'filters': {
            'responsible': {
                'label': 'ID исполнителя',
                'hidden': true,
                'value': '',
            },
            'author': {
                'label': 'ID постановщика',
            },
            'dateFrom': {
                'label': 'Срок исполнения задачи С',
                'type': 'date',
                'hidden': true,
                'value': '',
            },
            'dateTo': {
                'label': 'Срок исполнения задачи ПО',
                'type': 'date',
                'hidden': true,
                'value': '',
            },
            'pageNum': {
                'label': 'Номер страницы',
                'value': 1,
                'hidden': true,
            },
            'sort': {
                'label': 'Поле сортировки',
                'type': 'select',
                'value': 'status',
                'optionValues': ['status'],
                'hidden': true,
            },
            'sortType': {
                'label': 'Тип сортировки',
                'type': 'select',
                'value': 'ASC',
                'optionValues': ['DESC', 'ASC'],
                'hidden': true,
            },
            'status': {
                'label': 'Статусы',
                'type': 'multiSelect',
                'value': [],
                'optionValues': ['new', 'progress', 'done', 'check', 'date'],
                'multipleSelect': true,
            },
            'no_date': {
                'label': 'Показывать задачи без дат',
                'type': 'select',
                'value': '',
                'optionValues': ['', '1'],
                'hidden': true,
            },
            'limit': {
                'label': 'Записей на странице',
                'value': 5,
                'hidden': true,
            },
        },
    },
    'set_3': {
        'label': 'Задачи «новые» и «в работе»',
        'filters': {
            'responsible': {
                'label': 'ID исполнителя',
                'hidden': true,
                'value': '',
            },
            'author': {
                'label': 'ID постановщика',
                'hidden': true,
                'value': '',
            },
            'dateFrom': {
                'label': 'Срок исполнения задачи С',
                'type': 'date',
            },
            'dateTo': {
                'label': 'Срок исполнения задачи ПО',
                'type': 'date',
            },
            'pageNum': {
                'label': 'Номер страницы',
                'value': 1,
                'hidden': true,
            },
            'sort': {
                'label': 'Поле сортировки',
                'type': 'select',
                'value': 'status',
                'optionValues': ['status'],
                'hidden': true,
            },
            'sortType': {
                'label': 'Тип сортировки',
                'type': 'select',
                'value': 'ASC',
                'optionValues': ['DESC', 'ASC'],
                'hidden': true,
            },
            'status': {
                'label': 'Статусы',
                'type': 'multiSelect',
                'value': ['new', 'progress'],
                'optionValues': ['new', 'progress'],
                'multipleSelect': true,
            },
            'no_date': {
                'label': 'Показывать задачи без дат',
                'type': 'select',
                'value': '',
                'optionValues': ['', '1'],
            },
            'limit': {
                'label': 'Записей на странице',
                'value': 5,
                'hidden': true,
            },
        },
    },
};
});